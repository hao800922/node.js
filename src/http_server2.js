const http = require('http')
const fs = require('fs')
// 宣告fs 接收的套件fs (fs為file system 的縮寫)

const server = http.createServer(function (req, res) {
    fs.writeFile(__dirname + '/header01.txt',
    // 在 fs 上寫入檔案
        // 1. 在當前資料夾中創建檔案header01.txt
        JSON.stringify(req.headers),
        // 2. 將接收到的headers以JSON字串的方式寫入檔案
        function (error) {
            if (error) {
                console.log('error', error);
                res.end('error');
            } else {
                console.log('ok');
                res.end('ok')
            };
        // 3. 如果沒有發生錯誤, 回應客戶端ok
        });
});
server.listen(3000);