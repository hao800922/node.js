const express=require('express');
const router=express.Router()
const moment=require('moment-timezone');
const { query } = require('express');
const { render } = require('ejs');
const email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
const mobile_pattern = /^09-?\d{2}-?\d{3}-?\d{3}$/



const db=require(__dirname+'/../db_connect2')
// 加入database (也是外掛)


// 連結list部分------------------------------------------------
router.get('/list/:page?',async (req, res)=>{
    res.locals.pageName='address-book-list';
    let page=parseInt(req.params.page) || 1; // 如果':page?'不是數字, 就把他當第一頁
    // 字串經過parseInt()會變NaN, 沒打字會變undefined, 兩者 || 1, 結果都是1
    // 因此少了這個'/address-book/list'會404
    const perPage=5; // 每頁5筆

    const output={
        perPage,
        page,
        totalRows:0,
        totalPage:0,
        row:[]
    }
    // 先算出table總共多少rows
    const t_sql="SELECT COUNT(1) AS num FROM address_book";
    const [t_r]=await db.query(t_sql);
    output.totalRows=t_r[0].num;

    // 有了totalRows就可以算出totalPage
    output.totalPage=Math.ceil(output.totalRows/perPage);

    // 沒有資料時, 顯示list第一頁
    if(! output.totalRows) {
        return res.render('address-book/list',output);
    }
    // 加上return, if成立就不會往下走

    // 頁數小於1, 轉跳第一頁
    if(page<1) {
        return res.redirect('/address-book/list/1');
    }

    // 頁面大於最大頁數, 轉跳最後一頁
    if(page>output.totalPage) {
        return res.redirect('/address-book/list/'+output.totalPage);
    }



    const sql=`SELECT * FROM address_book ORDER BY sid DESC LIMIT ${(page-1)*perPage},${perPage}`;
    const [r]=await db.query(sql);
    r.forEach((el)=>{
        el.birthday=moment(el.birthday).format('YYYY-MM-DD')
    });
    output.rows=r;
    res.render('address-book/list',output);
});
//------------------------------------------------------------

// 連結add部分------------------------------------------------

// 呈現頁面
router.get('/add',(req, res)=>{
    res.locals.pageName='address-book-add';
    res.render('address-book/add');
});

// 接收資料
router.post('/add', async (req, res)=>{
    const output={
        success: false,
        body: req.body
    };

    // 資料檢查
    if(! email_pattern.test(req.body.email)){
        output.error = 'Email 格式不符';
        return res.json(output);
    };
    if(! mobile_pattern.test(req.body.mobile)){
        output.error = '手機號碼 格式不符';
        return res.json(output);
    };

    // 連結mysql
    const sql="INSERT INTO `address_book` SET ?";
    const [result]=await db.query(sql, [req.body]);

    // 判斷success
    if(result.affectedRows===1 && result.insertId) {
        output.success=true;
    }

    // 回傳用戶端
    output.result=result;
    res.json(output);

});
//------------------------------------------------------------


// 連結delete部分-------------------------------------------------
router.get('/del/:sid', async (req, res)=>{
    const sql="DELETE FROM `address_book` WHERE sid=?";
    const [result]=await db.query(sql, [req.params.sid]);

    // 刪除後的轉跳畫面
    if(req.get('Referer')) {
        res.redirect(req.get('Referer'));
    }else {
        res.redirect('/address-book/list');
    }
});
//----------------------------------------------------------------


// 連結修改部分----------------------------------------------------
router.get('/edit/:sid',async (req, res)=>{
    const sql="SELECT * FROM `address_book` WHERE sid=?" // ?為保留
    const [result]=await db.query(sql, [req.params.sid]) // []用來安插在?上
    if(result.length) { 
        // 只要有查詢到東西, result.length不是0, true 成立
        result[0].birthday=moment(result[0].birthday).format('YYYY-MM-DD');
        // 修改日期格式, 不然無法在<input type='date'>呈現
        res.render('address-book/edit',{row:result[0]});
    } else {res.redirect('/address-book/list')} 
});

router.post('/edit', async (req, res)=>{
    const output={
        success:false,
        body:req.body
    };
    // 資料檢查
    if(! email_pattern.test(req.body.email)){
        output.error = 'Email 格式不符';
        return res.json(output);
    };
    if(! mobile_pattern.test(req.body.mobile)){
        output.error = '手機號碼 格式不符';
        return res.json(output);
    };
    const updateData={...req.body};
    const sid=updateData.sid; // 把sid從群體中獨立取出 (後面用WHERE來篩選)
    delete updateData.sid;    // 群體中已無sid (因為sid並沒有要修改)
    const sql="UPDATE `address_book` SET ? WHERE sid=?";
    const [result]=await db.query(sql, [updateData, sid]);
    if(result.changedRows===1) {
        output.success=true;
    }
    output.result=result;
    res.json(output);
})
//-------------------------------------------


// 帳號登入------------------------------------
router.get('/login', (req, res)=>{
    res.locals.pageName='address-book-login'
    res.render('address-book/login')
});

router.post('/login', async (req, res)=>{
    const output={
        success:false,
        info:'帳號或密碼錯誤'
    };
    const sql="SELECT * FROM admins WHERE account=? AND password=SHA(?)";
    const [result]=await db.query(sql,[req.body.account, req.body.password])
    if(result.length) {
        req.session.adminUser=result[0]; // 將admin匹配到的user資料丟入req.session.adminUser
        output.success=true;
        output.info='';
    }
    res.json(output);
})
//--------------------------------------------


// 帳號登出------------------------------------
router.get('/logout', (req, res)=>{
    delete req.session.adminUser;
    res.send(`<script>location.href='/'</script>`);
})
//--------------------------------------------
module.exports=router