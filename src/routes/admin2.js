const express=require('express');
const router=express.Router()

router.get('/admin2/:action?/:id?', (req, res)=>{
    res.json({
        url:req.url,
        baseUrl:req.baseUrl,
        action:req.params.action,
        id:req.params.id
    });
});

module.exports=router;