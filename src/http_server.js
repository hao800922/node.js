const http=require('http'); // 指定常數http 為被接收的套件http

const server=http.createServer(function(req, res) {
    // 指定常數 server 為建立在常數http上的Server 函數如下

    res.writeHead(200,{
        'Content-Type':'text/html'
    });
    // 第一個回應:在接收到狀態碼為200(正常)時, response header中的Content-Type是HTML格式
    // 因此第一個回應並沒有出現實質東西, 只是讓下面res.end()有HTML格式, 可以把它註解掉看看
    // 1. 如果是'Content-Type': 'text/plain' 則Content-Type是文字格式
    // 2. 當前內容沒有其他狀態碼, 所以200就算寫其他狀態碼或不寫, 不會影響結果

    res.end(`
    <h2>Hello 123</h2>
    <h3>${req.url}</h3>
    `);
    // 第二個回應:回傳字串給客戶端
    // 1. server必須要有res.end()或類似方法, 不然browser得不到回應, 只會轉圈圈到死
    // 2. ``跟''與""一樣為字串, 不過``內部可用${}容納變數, 不需要+號連結, e.g. [var x ; `<h2>${x}</h2>`] 
    // 3. req.url為server端接收到的路由, 路由就是'/'後面的的東西
});

server.listen(3000);
// 當前server占用 Port 3000(先佔先贏)