// 呼叫套件或外掛js========================================================
const express = require('express');
const app = express();
// express起手式
// 套件express主要功用:
// 1. 簡化並取代內件套件http及其相關方法
// 2. 引入路由在適當時機取代相對路徑
// 在專案下安裝 >npm i express
// ref https://www.npmjs.com/package/express

const { v4: uuidv4 } = require('uuid');
// 引入uuidv4以供使用
// 套件uuid會使用到的功用:
// uuid中的v4為創建一個超多位數亂碼, 可作為不重複的代碼
// 在專案下安裝 >npm i uuid
// ref https://www.npmjs.com/package/uuid

// const multer=require('multer');
// const upload=multer({dest:'tmp_upload'}); // 上傳到目的地資料夾tmp_upload
// multer簡單起手式_記得還要在專案下建立資料夾
// 套件multer功用:
// 將用戶端的檔案上傳到指定資料夾
// 在專案下安裝 >npm i multer
// ref https://www.npmjs.com/package/multer

const session = require('express-session');
// 引入express-session
// 套件express-session功能:
// 1. 可以設定用戶端的閒置時間
// 2. 得知用戶端的session ID、查看用戶端使用資訊, e.g.登入次數、使用時間...
// 在專案下安裝 >npm i express-session
// ref https://www.npmjs.com/package/express-session

const MysqlStore = require('express-mysql-session')(session);
// 引入express-mysql-session
// 套件express-mysql-session功能:
// 存在記憶體的session資料, 備份到mysql上面
// 在專案下安裝 >npm i express-mysql-session


const moment = require('moment-timezone')
// 引入moment-timezone
// 套件moment-timezone功能:
// 可將時間套用各種格式
// 在專案下安裝 >npm i moment-timezone

const upload = require(__dirname + '/upload-module');
// 設定multer比較複雜時, 另外用upload-module.js檔分開裝
// 用戶端上傳檔案, 會經由外掛js檔處理

const db = require(__dirname + '/db_connect2');
// 呼叫外掛方式的database

const sessionStore = new MysqlStore({}, db)
// 將session存放在db之中
// 1. 記得要放在 const db 下面
// 2. 之所以有{}, 是因為全部用預設值即可

app.set('view engine', 'ejs');
// 啟動ejs_記得還要在專案下建立views資料夾
// views資料夾將成為主要存取位置,相對路徑的起始點
// 套件ejs功用:
// 1. 切割與連結一個完整html頁面, 使主架構相同的頁面, 變換內容更加容易
// 2. 跳脫以往格式的插入
// 3. views資料夾將成為主要存取位置,相對路徑的起始點
// 在專案下安裝 >npm i ejs
// vs code 記得安裝模組ESJ language support, 不然會出現語法錯誤提示
// ref https://www.npmjs.com/package/ejs
//===========================================================================


// Top-level meddleware----------------------------------------------------
app.use(express.urlencoded({ extended: false }));
// 啟動urlencoded()
// 用於form, 當用戶端以post的方式回傳queryString時, 轉匯成物件格式

app.use(express.json());
// 啟動json()
// 將讀取資料轉換成 json

app.use(session({
    secret: 'asdadftbtbre', // 亂打一串字
    saveUninitialized: false,
    resave: false,
    store: sessionStore,
    cookie: {
        maxAge: 1200000 // 最大閒置時間(msec)
        // 而如果要查詢何時斷線要用req.session.coockie.expires
    }
}));
// 當用戶進來網站時，會記錄session ID, 及其相關資料
//-----------------------------------------------------------------------------------

// custom middleware----------------------------------------------------------------
// 可以預設一些全域的變數，如果用戶修改將被取代
app.use((req, res, next)=>{
    res.locals.pageName=''; // 用來active按鈕
    res.locals.email='null';
    res.locals.password='0000';

    // req.session.adminUser={
    //     account:'hao',
    //     nickname:'晧'
    // };
    res.locals.sess=req.session;

 next()
})
//-----------------------------------------------------------------------------------


// 下面簡易編輯各路由對應頁面(小測試)===================================================

// 首頁小測試----------------------------------------------------------------
// app.get('/',function(req, res) {
//     res.send('<h2>Hello Express</h2>');
// });
// 經由路由'/'時, 簡單回應'字串'給用戶端, send()可辨識html標籤
// --------------------------------------------------------------------------


// 不實用的簡易表單小測試--------------------------------------------------------------
app.get('/try-post-form',(req, res)=>{
    res.render('try-post-form');
});
// 當路由為/try-post-form時, 回傳給用戶端畫面try-post-form.ejs
// -----------------------------------------------------------------------------------


// uuidv4小測試--------------------------------------------------
// app.get('/try-uuid', (req, res)=>{
//     res.json({
//         a:uuidv4(),
//         b:uuidv4()
//     });
// });
//---------------------------------------------------------------------


// 等候小測試-----------------------------------------------
app.get('/pending', (req, res) => { });
// 當路由為/pending時,不回應任何東西給用戶端，用戶端的畫面會一直等待
// 用在後面同步與非同步測試
// ---------------------------------------------------------


// 路由參數小測試(req.params)---------------------------------------------
app.get('/my-params1/:action?/:id?', (req, res) => {
    res.json({
        url: req.url,
        baseUrl: req.baseUrl,
        action: req.params.action,
        id: req.params.id,
        session: req.session,
    })
})
// 只要路由符合規則/my-params1/XXX/YYY
// 主要用途:
// 1. 大量資料需要很多頁面時, 利用路由參數加入運算, 不用自己寫一堆頁面
// 路由相關部分皆是req(因為是用戶端輸入的)

// 使用外掛方式
app.use('/admin', require(__dirname + '/routes/admin2'));
// 此時路由為index.js上的baseUrl'/admin'+admin2.js上的'/admin2/:action?/:id?'
// => '/admin/admin2/:action?/:id?'
//-----------------------------------------------------------


// 路由設定小測試(req.url)-----------------------------------------------
// 規則編碼可以在https://regex101.com 測試
// 編碼基本規則說明, ^為開頭, $為結尾, \d{n}為n個數字, [A-Z]{n}為n個大寫字母, ?代表前面字元可有可無
// 最後再用Regex flag結尾, e.g. /i 代表無大小寫區分
// 例如想呈現電話/m/09XXXXXXXX
// =>^\/m\/09\d{8}$/i   之中"/"有其他功用, 所以"\"放在"/"前用於跳脫, \/等於純字串"/"
// =>^\/m\/09\d{2}-?\d{3}-?\d{3}$/i  , 有些人喜歡用"-"區隔
app.get(/^\/m\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res) => {
    // 用此編碼時, 非字串, "/......"=>/.......
    res.json({
        url: req.url,
        params: req.params, // 寫法雖然很像params, 但其實無關, 抓不到
        手機: (req.url).slice(3).split('-').join('')
        // 切片位置3到最後, 取代'-',置換''(空字串)
    })
})
//-------------------------------------------------------------


// 查詢database(的table)小測試--------------------------------------------
app.get('/try-db', (req, res) => {
    const sql = "SELECT * FROM address_book";
    db.query(sql)
        .then(
            ([r]) => {
                res.json(r);
                console.log(r)

                // mysql中被搜尋表格資料會被[]包住,變成陣列
                // 此箭頭函數意即, 尋找陣列, 脫去[], 再轉變為json
            });


});

// 查詢database(的table)非同步寫法
app.get('/try-db2', async (req, res) => {
    const sql = "SELECT * FROM address_book";
    const [r] = await db.query(sql);
    // res.json(r) 只呈現json
    // console.log(r) 
    //-------------------------------------------------------
    res.render('address-book/list.ejs', { rows: r }) // 轉成表格
});
//--------------------------------------------------------


// 查詢網站登入次數-------------------------------------------
app.get('/try-session', (req, res) => {
    req.session.my_var = req.session.my_var || 0;
    // 一開始my_var沒有被定義, (undefined || 0)為0, 結論my_var=0
    req.session.my_var++;
    // my_var++後, my_var=1, 完成第一次計數
    //---------------------------------------------
    // 之後則是my_var=n(正整數),(n || 0)為n
    // my_var++後, my_var=n+1, 完成第n+1次計數
    //---------------------------------------------
    res.json({
        my_ver: req.session.my_var,
        session: req.session
    })
})
// my_var公式只做在這一路由, 因此只有照訪這頁, my_var才會增加
// 不過其他session資料是網站全域的
//---------------------------------------------------------------


// 時間格式小測試----------------------------------------------------------------
app.get('/try-moment',(req,res)=>{
    const fm='YYYY-MM-DD HH:mm:ss';
    const mo1=moment(req.session.cookie.expires); //
    const mo2=moment(new Date);
    res.json([
        mo1, // moment()為格林威治標準時間(+0), 跟沒有加moment()的結果是一樣的
        mo2, // 但是沒有加, 函數格式不對沒辦法進行下一步運算
        mo1.format(fm), // 1. 自動變更為當下時區-台灣時區(+8) 
        mo2.format(fm), // 2. 格式變為fm
        mo1.tz('Europe/London').format(fm), // 3.可改變時區-倫敦時區(+1)
        mo2.tz('Europe/London').format(fm),
    ])
})
//--------------------------------------------------------------------------




// 下面正式編輯各路由對應頁面===============================================


// 首頁--------------------------------------------------------------------
app.get('/', function (req, res) {
    res.locals.pageName='home';
    res.render('main', { name: 'Hao' })
});
// res.render('main',{name:'Hao'})
// 1. 經由路由'/'時, 回應用戶端main.ejs
// 2. {name:'Hao'}是連結到main.ejs中插入的<%= name %>, 可用於寫abstract
// --------------------------------------------------------------------------


// 外掛json 轉換成表格-----------------------------------------------------------
app.get('/json-sales', function (req, res) {
    // const data=require(__dirname+'/../data/sales.json'); 
    // res.render('json-sales',{sales:data});                 // (舊寫法)
    const sales = require(__dirname + '/../data/sales.json');
    res.render('json-sales', { sales })
});
// res.render('json-sales',{sales})
// 1. 經由路由'/json-sales'時, 回應用戶端json-sales.ejs, 並於頁面中安插{sales}
// 2. {sales}連結到json-sales.ejs中的<% for(let i in sales){...}%>
// 3. 因為在index.js中, 已用salse指定目標, 所以連結直接寫{salse}即可 (新寫法)
// --------------------------------------------------------------------------------


// 上傳單一圖檔 (可先用postman測試)-------------------------------------------------
app.post('/try-upload', upload.single('avatar'), function (req, res) {
    res.json(req.file)
})
// 當用戶端以POST方式回傳圖檔到路由'/try-upload'時,
// 圖檔經由index.js檢索,進入'upload-module.js'篩選、改變名稱、指定儲存路徑後,
// 伺服器端回應用戶端一個檔案資訊json
// ---------------------------------------------------------------------------------


// 上傳多個圖檔 (可先用postman測試)---------------------------------------------------
app.post('/try-upload-multi', upload.array('myphoto', 10), function (req, res) {
    res.json(req.files)
})
// upload.single對應到res.json(req.file)
// upload.array對應到res.json(req.files)
// -----------------------------------------------------------------------------------


// 簡易表單-------------------------------------------------------------------------
app.post('/try-post-form', (req, res) => {
    res.render('try-post-form', req.body);
});
// 當用戶端以POST方式回傳form到路由'/try-post-form'時,
// 伺服器端回應給用戶端'try-post-form.ejs'畫面，
// 並將用戶端之前回傳的form資訊(req.body)安插在'try-post-form.ejs'畫面對應位置之中


// 查詢有分頁的database的table
// router外掛(又外掛db, 又render('address-book/list'))
app.use('/address-book', require(__dirname + '/routes/address-book'));
// 在src/routes/address-book上設定的路由, 會再加上baseUrl'/address-book'
// 這邊為方便將路由設的跟views資料夾開始相對路徑一樣, 
// 但要注意的是views並不是public, 因此必須用get設定好路由, 才能顯現


//下面順序不要變動===================================================================


app.use(express.static('public'));
// 利用資料夾public中的XXX.html檔案名稱作為路由來呈現XXX.html

app.use(function (req, res) {
    res.status(404).send('<h2>404_找不到頁面</h2>')
});
// 未寫路由的status(404), 放在最後面

app.listen(3000, function () {
    console.log('server started');
});