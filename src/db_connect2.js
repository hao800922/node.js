const mysql=require('mysql2');
// 宣告mysql取得mysql2套件 
const pool=mysql.createPool({
    host:'localhost',
    user:'root',
    password:'P@ssw0rd',
    database:'test',
    waitForConnections:true,
    connectionLimit:10,
    queueLimit:0
});
// 建立一個pool, 丟入一個database

module.exports=pool.promise();